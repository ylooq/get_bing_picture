package org.idea.get_bing_picture.model

import org.idea.get_bing_picture.model.data.BingPicData
import java.net.HttpURLConnection
import java.net.URL
import com.alibaba.fastjson.*

class BingPicModel() {
    fun getPicUrl(): String  {
        var httpConn: HttpURLConnection = URL("http://cn.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1").openConnection() as HttpURLConnection
        var data: String = httpConn.inputStream.bufferedReader().readText()
        var bingPic: BingPicData = JSON.parseObject(data, BingPicData::class.javaObjectType)
        var bingPicUrl: String = "http://cn.bing.com" + bingPic.images.get(0).url.toString()
        print(bingPicUrl)
        return bingPicUrl

    }
}