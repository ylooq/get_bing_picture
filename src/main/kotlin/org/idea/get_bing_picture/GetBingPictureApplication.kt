package org.idea.get_bing_picture

import org.idea.get_bing_picture.model.BingPicModel
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@SpringBootApplication
class GetBingPictureApplication{
    @RequestMapping("")
    fun getBingPic():String{
        var bingPicModel=BingPicModel()
        return bingPicModel.getPicUrl()
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(GetBingPictureApplication::class.java, *args)
}
